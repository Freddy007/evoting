<?php

namespace App\Repositories\Contestant;

use App\Contestant;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;

class EloquentContestant extends EloquentRepository implements ContestantRepository
{
    protected $model;

    public function __construct(Contestant $contestant)
    {
        $this->model = $contestant;
    }

    public function all()
    {
        return $this->model->with("position")->get();
    }

    public function getContestantsByPosition($position_id)
    {
        return $this->model->wherePositionId($position_id)->get();
    }


    public function store(Request $request)
    {
        $contestant = parent::store($request);

//        if ($request->hasFile('image'))
//            $user->saveImage($request->file('image'), true, $user->id);

        return $contestant;
    }

    public function update(Request $request, $id)
    {
        $contestant = parent::update($request, $id);

        return $contestant;
    }

    public function destroy($contestant)
    {
        if(! $contestant instanceof Contestant)
            $contestant = parent::findTrash($contestant);

        return $contestant->forceDelete();
    }
}