<?php

namespace App\Repositories\Product;

use App\Position;
use App\Repositories\Position\PositionRepository;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;

class EloquentPosition extends EloquentRepository implements BaseRepository, PositionRepository
{
	protected $model;

	public function __construct(Position $position)
	{
		$this->model = $position;
	}

	public function all()
	{
    		return $this->model->get();
	}


    public function store(Request $request)
    {
        $position = parent::store($request);

        if ($request->hasFile('image'))
            $position->saveImage($request->file('image'), true, $position->id);

        return $position;
    }

    public function update(Request $request, $id)
    {
        $position = parent::update($request, $id);

        return $position;
    }

	public function destroy($position)
	{
        if(! $position instanceof Position)
            $position = parent::findTrash($position);

        return $position->forceDelete();
	}
}