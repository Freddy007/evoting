<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;

class EloquentUser extends EloquentRepository implements BaseRepository, UserRepository
{
	protected $model;

	public function __construct(User $user)
	{
		$this->model = $user;
	}

	public function all()
	{
    		return $this->model->get();
	}


    public function store(Request $request)
    {
        $user = parent::store($request);

//        if ($request->hasFile('image'))
//            $user->saveImage($request->file('image'), true, $user->id);

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = parent::update($request, $id);

        return $user;
    }

	public function destroy($user)
	{
        if(! $user instanceof User)
            $user = parent::findTrash($user);

        return $user->forceDelete();
	}
}