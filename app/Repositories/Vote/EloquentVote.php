<?php


namespace App\Repositories\Vote;


use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use App\Vote;
use Illuminate\Http\Request;

class EloquentVote extends EloquentRepository implements BaseRepository,VoteRepository
{

    protected $model;

    public function __construct(Vote $vote)
    {
        $this->model = $vote;
    }

    public function all()
    {
        return $this->model->get();
    }


    public function store(Request $request)
    {
        $vote = parent::store($request);
        $old_vote = Vote::find($vote->id);
        $old_vote->update(["count" => 1]);
        return $vote;
    }

    public function update(Request $request, $id)
    {
        $user = parent::update($request, $id);

        return $user;
    }

    public function processVote(Request $request)
    {
        $record_exists = Vote::wherePositionId($request->position_id)
            ->whereContestantId($request->contestant_id)->first();

        if ($record_exists){
            $record_exists->update([
                "count" => $record_exists->count+1
            ]);
        }else{
            $this->store($request);
        }
    }

    public function destroy($vote)
    {
        if(! $vote instanceof Vote)
            $vote = parent::findTrash($vote);

        return $vote->forceDelete();
    }

    public function getVotesByPosition($position_id){
        return $this->model->with(["position","contestant"])->wherePositionId($position_id)->get();
    }

    public function findVoteCountByContestant($contestant_id,$position){
        if ($vote = $this->model->whereContestantId($contestant_id)->wherePositionId($position)->first()){
            return $vote->count;
        }

        return 0;
    }
}