<?php


namespace App\Repositories\UserVoteTracker;


use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use App\UserVoteTracker;
use App\Vote;
use Illuminate\Http\Request;

class EloquentUserVoteTracker extends EloquentRepository implements BaseRepository, UserVoteRepository
{

    protected $model;

    public function __construct(UserVoteTracker $voteTracker)
    {
        $this->model = $voteTracker;
    }

    public function all()
    {
        return $this->model->with(["position","user"])->get();
    }


    public function store(Request $request)
    {
        $vote = parent::store($request);
        return $vote;
    }

    public function checkUserVote(Request $request)
    {
        $vote = $this->model->wherePositionId($request->position_id)
            ->whereUserId($request->user_id)->first();
        return $vote;
    }
}