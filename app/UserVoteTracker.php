<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVoteTracker extends Model
{
    //
    protected $table = "user_votes_tracker";

    protected $fillable = ["id","user_id","position_id"];

    public function position(){
        return $this->belongsTo(Position::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
