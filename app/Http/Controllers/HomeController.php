<?php

namespace App\Http\Controllers;

use App\Position;
use App\Repositories\Contestant\ContestantRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserVoteTracker\UserVoteRepository;
use App\Repositories\Vote\VoteRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    private $positionModel;
    private $userModel;
    private $contestantModel;
    private $voteModel;
    private $voteTrackerModel;

    private $position;
    private $user;
    private $contestant;
    private $vote;
    private $voteTracker;

    /**
     * Create a new controller instance.
     *
     * @param PositionRepository $position
     * @param UserRepository $user
     * @param ContestantRepository $contestant
     * @param VoteRepository $voteRepository
     */
    public function __construct(
        PositionRepository $position,
        UserRepository $user,
        ContestantRepository $contestant,
        VoteRepository $vote, UserVoteRepository $voteTracker)
    {
//                $this->middleware('auth');

        parent::__construct();
        $this->positionModel = trans('app.model.position');
        $this->userModel = trans('app.key.user');
        $this->contestantModel = trans('app.key.contestant');
        $this->voteModel = trans('app.key.vote');
        $this->voteTracker = trans('app.key.user_vote_tracker');

        $this->position = $position;
        $this->user = $user;
        $this->contestant = $contestant;
        $this->vote = $vote;
        $this->voteTracker = $voteTracker;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($id=1)
    {
        $query_id = $id != null ? $id : 1;
        $contestants = $this->contestant->getContestantsByPosition($query_id);
        $positions = $this->position->all();
        $users = $this->user->all();

        $next_position = Position::where('id', '>', $id)->min('id');


        if (\request()->ajax()){
            $collection = new Collection();

            foreach ($contestants as $cont) {
                $votesCount = $this->vote->findVoteCountByContestant($cont->id,$query_id);
                $votesArray = ['name' => $cont->name, 'y' => $votesCount];
                $collection->add($votesArray);
            }

            return response()->json(["votes" => $collection],200);
        }


        return view('index', compact("positions","users","contestants",'query_id','next_position'));
    }

    public function postVote(Request $request){
        $user_id = \Auth::id();
        $contestant_id = \request()->get("contestant_id");
        $position = \request()->get("position_id");
        $access_token = $request->get('access_token');
        $request->merge(['user_id' => $user_id]);

        if ($this->voteTracker->checkUserVote($request)){
            return response()->json(["message" => "You have already voted for this position!"],403);
        }

        $this->vote->processVote($request);
        $this->voteTracker->store($request);

        return response()->json(["contestant" => $contestant_id, "user_id" => $user_id, "position" => $position]);
    }

    public function getUserPhoneNumber()
    {
        return view('phone');
    }
}
