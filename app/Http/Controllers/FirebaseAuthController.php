<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Http\Request;

class FirebaseAuthController extends Controller
{

    public function login(Request $request) {

    // Launch Firebase Auth
    $auth = app('firebase.auth');
    // Retrieve the Firebase credential's token
    $idTokenString = $request->input('firebase_token');
    $phoneNumber = $request->input('phone_number');


    try { // Try to verify the Firebase credential token with Google

        $verifiedIdToken = $auth->verifyIdToken($idTokenString);

    } catch (\InvalidArgumentException $e) { // If the token has the wrong format

        return response()->json([
            'message' => 'Unauthorized - Can\'t parse the token: ' . $e->getMessage()
        ], 401);

    } catch (InvalidToken $e) { // If the token is invalid (expired ...)

        return response()->json([
            'message' => 'Unauthorized - Token is invalid: ' . $e->getMessage()
        ], 401);

    }

        $uid = $verifiedIdToken->getClaim('sub');

        $account_exists = User::wherePhoneNumber($phoneNumber)->first();

    if ($account_exists) {

        $account_exists->update([
            "firebase_uid" => $uid
        ]);

        // Retrieve the user model linked with the Firebase UID
//        $user = User::where('firebase_uid', $uid)->first();

        $tokenResult = $account_exists->createToken('UserToken');

        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        \Auth::loginUsingId($account_exists->id,true);

           return response()->json([
               'id' => $account_exists->id,
               'access_token' => $tokenResult->accessToken,
               'token_type' => 'Bearer',
               'expires_at' => Carbon::parse(
                   $tokenResult->token->expires_at
               )->toDateTimeString()
           ]);
    }

    return response()->json(["message" => "Unauthorized to vote!"], 401);
 }
}
