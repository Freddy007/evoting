<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserInterest;
use Illuminate\Support\Facades\Auth;
use Validator;
class UserController extends Controller
{
    public $dataStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $data['userId'] = $user->id;
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json([ $data], $this->successStatus, ["x-auth-token" => [$data['token']]]);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $data['token'] =  $user->createToken('MyApp')-> accessToken;
        $data['name'] =  $user->name;
        return response()->json([$data], $this->successStatus);
    }

}
