<?php

namespace App\Http\Controllers;

use App\Repositories\Position\PositionRepository;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    //

    private $model;

    private $position;

    /**
     * construct
     */
    public function __construct(PositionRepository  $position)
    {
        parent::__construct();
        $this->model = trans('app.model.position');
        $this->$position = $position;
    }

}
