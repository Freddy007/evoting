<?php

namespace App\Providers;

use App\Repositories\Contestant\ContestantRepository;
use App\Repositories\Contestant\EloquentContestant;
use App\Repositories\Position\PositionRepository;


use App\Repositories\Product\EloquentPosition;

use App\Repositories\User\EloquentUser;
use App\Repositories\User\UserRepository;
use App\Repositories\UserVoteTracker\EloquentUserVoteTracker;
use App\Repositories\UserVoteTracker\UserVoteRepository;
use App\Repositories\Vote\EloquentVote;
use App\Repositories\Vote\VoteRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(
            PositionRepository::class,
            EloquentPosition::class
        );

        $this->app->singleton(
            UserRepository::class,
            EloquentUser::class
        );

        $this->app->singleton(
            ContestantRepository::class,
            EloquentContestant::class
        );

        $this->app->singleton(
            VoteRepository::class,
            EloquentVote::class
        );


        $this->app->singleton(
            UserVoteRepository::class,
            EloquentUserVoteTracker::class
        );
    }

}
