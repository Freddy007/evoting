<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{

    protected $fillable = ["id","name","image","color","description","position_id"];

    public function position(){
        return $this->belongsTo(Position::class);
    }
}
