<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{

    protected $fillable = ["position_id","contestant_id","count","percentage"];


    public function position(){
        return $this->belongsTo(Position::class);
    }

    public function contestant(){
        return $this->belongsTo(Contestant::class);
    }
}
