-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 25, 2020 at 01:14 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evoting`
--

-- --------------------------------------------------------

--
-- Table structure for table `contestants`
--

CREATE TABLE `contestants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position_id` bigint(20) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contestants`
--

INSERT INTO `contestants` (`id`, `name`, `image`, `color`, `description`, `position_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Person 1', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 1, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(2, 'Person 2', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 1, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(3, 'Person 3', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 1, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(4, 'Person 4', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 1, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(5, 'Person 5', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 2, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(6, 'Person 6', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 2, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(7, 'Person 7', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 2, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(8, 'Person 8', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 2, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(9, 'Person 9', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 3, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(10, 'Person 10', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 3, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(11, 'Person 11', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 3, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(12, 'Person 12', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 3, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(13, 'Person 13', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 4, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(14, 'Person 14', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 4, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(15, 'Person 15', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 4, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(16, 'Person 16\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 4, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(17, 'Person 17\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 5, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(18, 'Person 18\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 5, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(19, 'Person 19\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 5, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(20, 'Person 20\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 5, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(21, 'Person 21\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 6, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(22, 'Person 22\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 6, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(23, 'Person 23\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 6, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(24, 'Person 24\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 6, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(25, 'Person 25\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 7, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(26, 'Person 26\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 7, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(27, 'Person 27\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 7, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(28, 'Person 28\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 7, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(29, 'Person 29\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 8, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(30, 'Person 30\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 8, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(31, 'Person 31\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 8, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(32, 'Person 32\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 8, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(33, 'Person 33\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 9, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(34, 'Person 34\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 9, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(35, 'Person 35\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 9, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(36, 'Person 36\r\n\r\n', 'https://hips.hearstapps.com/ell.h-cdn.co/assets/16/30/1469916253-game-of-thrones-daenerys-targaryen-emilia-clarke.jpg?crop=0.532xw:0.946xh;0.291xw,0.0514xh&resize=768:*', NULL, NULL, 9, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_23_072609_create_positions_table', 1),
(5, '2020_12_23_081014_create_contestants_table', 1),
(6, '2020_12_23_081854_create_user_votes_tracker_table', 1),
(7, '2020_12_23_082543_create_votes_table', 1),
(8, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(9, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(10, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(11, '2016_06_01_000004_create_oauth_clients_table', 2),
(12, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(13, '2020_12_24_073120_add_firebase_uid_to_users_table', 3),
(14, '2020_12_24_204154_add_colors_and_description', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('123fd2b858d933666b5e3c85373225f55ebd396065376b1fd4a2700c8a0b0d88cc05e77719f27fe2', 3, 1, 'UserToken', '[]', 0, '2020-12-24 17:36:37', '2020-12-24 17:36:37', '2020-12-31 17:36:37'),
('13063c09f534e2751b27a69e4222a319fd7993da41dfe471add826902f496e447c7cae23cf508d4a', 1, 1, 'UserToken', '[]', 0, '2020-12-24 18:55:19', '2020-12-24 18:55:19', '2020-12-31 18:55:19'),
('230eefdb9bb0d2e359f074ff14507341119aaebc0ad21220b314a0bcee8bd67bb0eb500cd866a5d0', 2, 1, 'UserToken', '[]', 0, '2020-12-24 19:06:15', '2020-12-24 19:06:15', '2020-12-31 19:06:15'),
('52356d96f2234c852ffbe3f32d945330c51da775f797fea79a85e019dd596b2d6172bb3b23af60a4', 3, 1, 'UserToken', '[]', 0, '2020-12-24 17:51:09', '2020-12-24 17:51:09', '2020-12-31 17:51:09'),
('5e3d6d300eeb4f458be0dbe9f609c43e08902574fb9fb94a22adc3a9b38da159d366ad4aa5f47bac', 1, 1, 'UserToken', '[]', 0, '2020-12-24 18:57:57', '2020-12-24 18:57:57', '2020-12-31 18:57:57'),
('67ea0632a081c066423e4248ef957af183c780c7d27edd84aeec25f8cdbc1a08ec188f7eeeefcc77', 2, 1, 'UserToken', '[]', 0, '2020-12-24 23:46:11', '2020-12-24 23:46:11', '2020-12-31 23:46:12'),
('6e79fe53513fb7a44ee336439b38b9d0b49fc89d349e8a32c98ee6dc3b2e7e581c54a0c790e46a09', 3, 1, 'UserToken', '[]', 0, '2020-12-24 17:34:45', '2020-12-24 17:34:45', '2020-12-31 17:34:45'),
('797c28b78f8bb7c86e1d0ff9b613deaeefb08fd16aad6578b0e905b3f6c22ef133f82650160c9d2b', 3, 1, 'UserToken', '[]', 0, '2020-12-24 17:42:20', '2020-12-24 17:42:20', '2020-12-31 17:42:20'),
('9fec286ff2c516d839546f003572a0fbb4eee8327d19e45a124df98754eae652568579578bbf7f2f', 3, 1, 'UserToken', '[]', 0, '2020-12-25 00:46:17', '2020-12-25 00:46:17', '2021-01-01 00:46:17'),
('d1eba5a1745db81e09a4ec5e823685df9542a812ccfdb35c3e1b62db1a533b16bfcb666168205474', 3, 1, 'UserToken', '[]', 0, '2020-12-24 17:40:53', '2020-12-24 17:40:53', '2020-12-31 17:40:53'),
('dca7895124596092ed9a85968f3db4162f514cf8914e603115422a62fe2046c3613ad4c45c887f59', 2, 1, 'UserToken', '[]', 0, '2020-12-24 19:48:17', '2020-12-24 19:48:17', '2020-12-31 19:48:17'),
('f1b180fc0bf72eb0c0dbf00201d8b37bd84ae5d5c9e1926fe1544fc7619c0d0cdb5f41517447f4ac', 3, 1, 'UserToken', '[]', 0, '2020-12-25 00:56:04', '2020-12-25 00:56:04', '2021-01-01 00:56:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'XtraClass Personal Access Client', 'gXzIYvIbYgCJ632KuUshqXNnsVVVHQDdMcIsYkhG', NULL, 'http://localhost', 1, 0, 0, '2020-12-24 07:24:54', '2020-12-24 07:24:54'),
(2, NULL, 'XtraClass Password Grant Client', 'q52kqeWCYEFzLdN38EJRsQq54Wc719yna9Ig5KHZ', 'users', 'http://localhost', 0, 1, 0, '2020-12-24 07:24:54', '2020-12-24 07:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-24 07:24:54', '2020-12-24 07:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `order`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Presidential', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(2, 'Vice President', 1, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(3, 'General Secretary', 3, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(4, 'Financial Secretary', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(5, 'Public Relations', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(6, 'General Organizer', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(7, 'Cordinating Secretary', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(8, 'Secretary for Education', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(9, 'Press and Information', 0, 1, '2020-12-23 00:00:00', '2020-12-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firebase_uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone_number`, `email`, `email_verified_at`, `password`, `verification_token`, `firebase_uid`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Adjoa Frimpong', '+233238860038', NULL, NULL, NULL, NULL, NULL, '2LD6V5E4BzwMXor9PkfHHeaifvyCtFZFevM4srtQuQCjJRzR4WlFRx9bAQBC', '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(2, 'Kofi Baboni', '+233235297348', NULL, NULL, NULL, NULL, NULL, 'OPgqQqTqYfKqO57uEOaXPkoQ4AwTqA89hawaLuymwhMyO8GDdTd3bbG5tXz2', '2020-12-23 00:00:00', '2020-12-23 00:00:00'),
(3, 'Frederick Ankamah', '+233242953672', 'freddy100dollar@gmail.com', NULL, '$2y$10$HYKlRj3foCoDBg/RoeJyoe1WyavUL5PyTfcs5qiLnOYmIE7TO0Fxy', NULL, NULL, 'Y0d45MvHv1rCWjOwCe30aLlEU3oc5deTw0hA1DHl48vXbGMufBijdvhPTymJ', '2020-12-24 16:40:42', '2020-12-24 16:40:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_votes_tracker`
--

CREATE TABLE `user_votes_tracker` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `position_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_votes_tracker`
--

INSERT INTO `user_votes_tracker` (`id`, `user_id`, `position_id`, `created_at`, `updated_at`) VALUES
(20, 3, 1, '2020-12-25 00:56:16', '2020-12-25 00:56:16'),
(21, 3, 2, '2020-12-25 00:56:29', '2020-12-25 00:56:29'),
(22, 3, 3, '2020-12-25 00:56:41', '2020-12-25 00:56:41'),
(23, 3, 4, '2020-12-25 00:56:53', '2020-12-25 00:56:53'),
(24, 3, 5, '2020-12-25 00:57:03', '2020-12-25 00:57:03'),
(25, 3, 6, '2020-12-25 00:57:13', '2020-12-25 00:57:13'),
(26, 3, 7, '2020-12-25 00:57:25', '2020-12-25 00:57:25'),
(27, 3, 8, '2020-12-25 00:57:40', '2020-12-25 00:57:40'),
(28, 3, 9, '2020-12-25 00:57:50', '2020-12-25 00:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position_id` bigint(20) UNSIGNED NOT NULL,
  `contestant_id` bigint(20) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `percentage` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`id`, `position_id`, `contestant_id`, `count`, `percentage`, `created_at`, `updated_at`) VALUES
(39, 1, 1, 1, 0, '2020-12-25 00:56:16', '2020-12-25 00:56:16'),
(40, 2, 8, 1, 0, '2020-12-25 00:56:29', '2020-12-25 00:56:29'),
(41, 3, 11, 1, 0, '2020-12-25 00:56:41', '2020-12-25 00:56:41'),
(42, 4, 15, 1, 0, '2020-12-25 00:56:53', '2020-12-25 00:56:53'),
(43, 5, 19, 1, 0, '2020-12-25 00:57:03', '2020-12-25 00:57:03'),
(44, 6, 22, 1, 0, '2020-12-25 00:57:13', '2020-12-25 00:57:13'),
(45, 7, 26, 1, 0, '2020-12-25 00:57:25', '2020-12-25 00:57:25'),
(46, 8, 29, 1, 0, '2020-12-25 00:57:40', '2020-12-25 00:57:40'),
(47, 9, 33, 1, 0, '2020-12-25 00:57:50', '2020-12-25 00:57:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contestants`
--
ALTER TABLE `contestants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contestants_position_id_foreign` (`position_id`),
  ADD KEY `contestants_name_index` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `positions_name_index` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_votes_tracker`
--
ALTER TABLE `user_votes_tracker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_votes_tracker_user_id_foreign` (`user_id`),
  ADD KEY `user_votes_tracker_position_id_foreign` (`position_id`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `votes_position_id_foreign` (`position_id`),
  ADD KEY `votes_contestant_id_foreign` (`contestant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contestants`
--
ALTER TABLE `contestants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_votes_tracker`
--
ALTER TABLE `user_votes_tracker`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contestants`
--
ALTER TABLE `contestants`
  ADD CONSTRAINT `contestants_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_votes_tracker`
--
ALTER TABLE `user_votes_tracker`
  ADD CONSTRAINT `user_votes_tracker_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_votes_tracker_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_contestant_id_foreign` FOREIGN KEY (`contestant_id`) REFERENCES `contestants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `votes_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
