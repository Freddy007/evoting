@extends('layouts.home_layout')

@section('content')

    @php
    if (Auth::check()){
        $user_id = Auth::id();
        $voted = \App\UserVoteTracker::wherePositionId($query_id)->whereUserId($user_id)->first();
    }

    @endphp

    <div class="container">
        <div id="main-content">

        <div id="banner" class="img-fluid"></div>

        <div id="member-btns" style="position: relative; top: -29px; left: 7%">
            <button type="button">Core Members</button>
            <button type="button" class="members-button-two">Auxiliary Members</button>

            <div class="row" id="vote-categories" >
                <div class="col-md-3 col-lg-3 col-sm-3">

                    {!! $PositionsNav->asUl(array('class' => 'nav navbar-nav')) !!}

                </div>

                <div class="col-md-8">

                    <div>

                        @if(Auth::check() && $voted)

                            <figure class="highcharts-figure">
                                <div id="container"></div>
                                <p class="highcharts-description">
                                </p>
                            </figure>
                        @else

                            <div class="row text-center text-lg-left ml-auto">

                            @foreach($contestants as $contestant)

                            <div class="col-lg-5 col-sm-8 image-holder">
                                <figure class="figure">
                                    <img src="{{$contestant->image}}" width="250" height="200"
                                         class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
                                    <figcaption class="figure-caption">{{ $contestant->name }}</figcaption>
                                    <figcaption class="figure-caption">{{ $contestant->position->name }}</figcaption>
                                    <figcaption class="figure-caption last-figure-caption" style="background-color: blue; height: 30px;">
                                        Adjoa the leader</figcaption>

                                    <div class="overlay">
                                        <img src="{{asset("images/thumb.png")}}" style="position:relative; top: 5% " width="170" height="170"/>
                                        @if(Auth::check())
                                        <div class="vote-text text" style="border: 1px solid #fff; padding: 1px 10px 1px 10px;"
                                             data-id="{{$contestant->id}}" onclick="vote('{{$contestant->id}}','{{$contestant->position->id}}')">VOTE
                                        </div>
                                            @else
                                            <div class="vote-text text" style="border: 1px solid #fff; padding: 1px 10px 1px 10px;"><a href="/user/phone">VOTE</a></div>
                                        @endif
                                    </div>

                                </figure>

                            </div>
                            @endforeach
                    </div>

                        @endif

                </div>
            </div>

        </div>
        </div>
        </div>
    </div>
@endsection
