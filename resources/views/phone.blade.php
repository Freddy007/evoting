@extends('layouts.no_nav_layout')

@section('content')

    <div class="container">
        <div class="login-signup px-3 px-md-0">

            <div id="firebaseui-auth-container"></div>
            <div id="loader">Loading...</div>

        </div>
    </div>
@endsection
