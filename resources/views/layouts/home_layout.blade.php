<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'XtraClass') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('css/main.css') }}" rel="stylesheet"/>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


    <style>

        #banner{
            background: url(images/background.jpg) no-repeat fixed center ;
        }

        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 90%;
            opacity: 0;
            transition: .5s ease;
            background-color: rgba(0,0,0,0.7);
            border-radius: 7px;
            margin: auto;
            text-align: center !important;

        }

        .figure:hover .overlay {
            opacity: 1;
        }

        .text {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 80%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .text-lg-left {
            text-align: center !important;
        }

        .box {
            height: 200px;
            width: 300px;
            overflow: hidden;
            border: 1px solid red;
            background: #ff0;
        }
        .hid-box {
            top: 100%;
            position: relative;
            transition: all .3s ease-out;
            background: #428bca;
            height: 100%;
        }

        .box:hover > .hid-box{
            top: 0;
        }

        a{
            color: #ffffff;
            text-decoration: none;
        }

        /* mouse over link */
        a:hover {
            text-decoration: none;
            color: #fff;
        }

        .active{
            background-color: #0AC92B	;
        }



    </style>



</head>
<body>
<div id="app">
    <div class="container">
    <nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'XtraClass') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('phone') }}">{{ __('Login') }}</a>
                        </li>

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>


                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    </div>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>

    const next_position = "{{$next_position}}";
    const accessToken = localStorage.getItem("firebase_access_token");
    const voteButtonElement = document.querySelector('.text');
    console.log(accessToken);

    function vote(contestant_id, position_id){

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to change your vote again!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {

            if (result.isConfirmed) {

                axios.post('/vote', {
                    contestant_id: contestant_id,
                    position_id: position_id,
                    access_token: accessToken
                }).then(function (response) {
                    Swal.fire(
                        'Voted!',
                        'You have successfully voted!.',
                        'success'
                    );


                    setTimeout(function () {
                        location.href=`/${next_position}`;
                    },2000);

                    console.log("then..... " + response);
                }).catch(function (error) {
                        console.log("Catch..." + error);

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "You can't vote twice!",
                        footer: '<a href>Why do I have this issue?</a>'
                    })
                    });
            }

        });
    }

    function initiliazeChart(votes) {
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: "Number of Votes per Contestant"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} votes</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        // format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        format: '<b>{point.name}</b>: {point.y} votes'
                    }
                }
            },
            series: [{
                name: 'Votes',
                colorByPoint: true,
                data: votes
            }]
        });
    }

    setTimeout(function () {
        axios.get('')
            .then(function (response) {
                // handle success
                console.log(response);

                initiliazeChart(response.data.votes);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    },1000);


</script>
</body>
</html>
