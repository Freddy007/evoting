<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'XtraClass') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/main.css') }}" rel="stylesheet"/>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
    <script src="https://www.gstatic.com/firebasejs/ui/4.6.1/firebase-ui-auth.js"></script>
    <link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.6.1/firebase-ui-auth.css" />

    <style>

        body{
            background-color: #F8F9FA !important;
        }
    </style>

</head>
<body>
<div id="app">
    <div class="container">

    </div>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-firestore.js"></script>



<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyC-g33qaIaiSPSBy0xXqaD3HP_Cn5dWdgk",
        authDomain: "notional-impact-104217.firebaseapp.com",
        databaseURL: "https://notional-impact-104217.firebaseio.com",
        projectId: "notional-impact-104217",
        storageBucket: "notional-impact-104217.appspot.com",
        messagingSenderId: "757754181781",
        appId: "1:757754181781:web:f0833b32ffa0d6c6a7839c"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>

<script>

    let user_id = "";
    let ui = new firebaseui.auth.AuthUI(firebase.auth());

    let uiConfig = {
        callbacks: {
            signInSuccessWithAuthResult: function(authResult, redirectUrl) {

                const phoneNumber =  authResult.user.phoneNumber;

                console.log( authResult.user);
                console.log( authResult.user.phoneNumber);

                authResult.user.getIdToken()
                    .then(function(accessToken) {

                        localStorage.clear();
                        localStorage.setItem("firebase_access_token", accessToken);

                        axios.post('/user/send-login-token', {
                            firebase_token: accessToken,
                            phone_number:phoneNumber
                        })
                            .then(function (response) {
                                user_id = response.data.id;
                                console.log("response user id" + response.data.id);
                                localStorage.setItem("user_id", user_id);
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            }).then(function () {

                                setTimeout(function () {
                                    location.href="/1";
                                },2000);
                        });
                    });

                return false;
            },
            uiShown: function() {
                // The widget is rendered.
                // Hide the loader.
                document.getElementById('loader').style.display = 'none';
            }
        },
        // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
        signInFlow: 'popup',
        signInSuccessUrl: '/1',
        signInOptions: [
            // Leave the lines as is for the providers you want to offer your users.
            firebase.auth.PhoneAuthProvider.PROVIDER_ID
        ],
        // Terms of service url.
        tosUrl: '<your-tos-url>',
        // Privacy policy url.
        privacyPolicyUrl: ''
    };

    // The start method will wait until the DOM is loaded.
    ui.start('#firebaseui-auth-container', uiConfig);

    // window.onbeforeunload = function(){
    //     return 'Are you sure you want to leave?';
    // };
    //]]>
</script>
</body>
</html>
