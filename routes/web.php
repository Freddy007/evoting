<?php

use App\Position;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/{id}', 'HomeController@getIndex')->name('home');

Route::get('/', function () {
    return redirect('/1');
});

Route::get('/position/{id}', 'HomeController@getPosition')->name('position');

Route::get('/user/phone', 'HomeController@getUserPhoneNumber')->name('phone');

Route::post('/vote', 'HomeController@postVote')->name('vote');

Route::post('/user/send-login-token',"FirebaseAuthController@login");

Menu::make('PositionsNav',function($menu){
    $position =  Position::orderBy("created_at","asc");
    $navigation_menu =  $position->pluck("name")->toArray() ;
    $navigation_menu_id = $position->pluck("id")->toArray();
    $navigation_links = array_combine($navigation_menu, $navigation_menu_id);

    foreach($navigation_menu as $nav_menu){
         $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'vote-category'));;
    }
});